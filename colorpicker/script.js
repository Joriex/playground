let timer = 0;
let score = 0;

let intervalId = 0;
const startGame = (startButton) => {
    const numOfButtons = document.getElementById("numberOfButtons").value;
    console.log(numOfButtons);
    if(numOfButtons === '' || numOfButtons === '0' || numOfButtons === '1') return(alert("Give a num of buttons > 1"))
    
    document.getElementById("pregame").style.display = "none";
    document.getElementById("game").style.display = "block";

    generateButtons(numOfButtons)
    initializeButtonsAndBox(numOfButtons);
    intervalId = setInterval(setTimer, 10);
}


const generateButtons = (numOfButtons) => {
    const buttonContainer = document.getElementById("buttons__container");
    for(let i = 0; i < numOfButtons; i++){
        const button = document.createElement("button");
        button.classList.add("possibleColor__button");
        button.id = i.toString();
        button.setAttribute('onclick', 'testButton(this)');
        console.log(buttonContainer);
        buttonContainer.appendChild(button);
    }
}

const initializeButtonsAndBox = (numOfButtons) => {
    const color_box = document.getElementById("color__box");
    const box_rgb = [Math.round(Math.random() * 256), Math.round(Math.random() * 256), Math.round(Math.random() * 256)]
    color_box.style.backgroundColor= "rgb(" + box_rgb[0].toString() + "," + box_rgb[1].toString() + "," + box_rgb[2].toString() + ")";

    for(let i = 0; i < numOfButtons; i++){
        const button =  document.getElementById(i.toString());
        const button_rgb = [Math.round(Math.random() * 256), Math.round(Math.random() * 256), Math.round(Math.random() * 256)]
        button.style.backgroundColor = "rgb(" + button_rgb[0].toString() + "," + button_rgb[1].toString() + "," + button_rgb[2].toString() + ")"
    }

    const buttonToClick = document.getElementById(Math.round(Math.random() * (numOfButtons - 1)).toString());

    buttonToClick.style.backgroundColor = "rgb(" + box_rgb[0].toString() + "," + box_rgb[1].toString() + "," + box_rgb[2].toString() + ")"
}

const testButton = (clickedButton) => {
    const rgb_button = clickedButton.style.backgroundColor;
    const rgb_box = document.getElementById("color__box").style.backgroundColor;

    if(rgb_button === rgb_box){
        updateScore("plus");
        initializeButtonsAndBox(document.getElementById("numberOfButtons").value);
    }
    else{
        updateScore("minus");
        initializeButtonsAndBox(document.getElementById("numberOfButtons").value);
    }
}
const updateScore = (action) => {
    action === "plus" ? score += 1 : score -=1;
    document.getElementById("score").innerText = score.toString();
}

const setTimer = () =>{
    if(timer >= 10){
        clearInterval(intervalId);
        document.getElementById("game").style.display = "none";
        document.getElementById("postgame").style.display="flex";
        document.getElementById("postgameScore").innerText=score;
    }
    timer += 0.01;
    document.getElementById("timer").innerText = timer.toFixed(2).toString();
}