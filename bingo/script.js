const fieldSize = 4;
const strings = [
    "Ehm ja Ehm",
    "Das kennen sie ja schon aus der Schule",
    "Gembris Lebensweisheiten",
    "*Bei Uebungen über die Schulter schauen*",
    "Wer braucht das Arbeitsblatt?",
    "ist doch einfach",
    "Durch Folien rasen",
    "Naja nicht ganz",
    "*laeuft mit Unterschriftliste zu jeder Reihe",
    "Ehhhh...",
    "Formel aus dem nichts herleiten",
    "Klimaschutz/seine Projekte",
    "Gibt es noch Fragen/Anmerkungen?",
    "pruefungsrelevant",
    "Muesste ich noch einmal nach schauen",
    "*bei einem ernsten Thema laecheln",
    "Video anschauen",
    "Smartphones sind mobile Physiklabore",
    "Mhhh...",
    "komisches Lachen",
    "*zieht Spukke durch die Zaehne*",
    "Yanyellow",
    "Und ja...",
    "*Bringt Experiment mit*",
    "Weiss das jemand?",
    "GeoGebra benutzen",
    "Fragen?"
];

let usedStrings = [];
const field = document.getElementById("field");


const toggleStatus = (div) => {
    console.log("hsabda");
    if(div.style.backgroundColor !== "lightgreen"){
        div.style.backgroundColor = "lightgreen";
    }
    else{
        div.style.backgroundColor = "";
    }
}
const createFields = () => {
    for(let i = 0; i < fieldSize; i++){
        for(let j = 0; j < fieldSize; j++){
            let wordIndex = Math.round(Math.random() * (strings.length - 1));
            while(usedStrings.includes(strings[wordIndex])){
                wordIndex =  Math.round(Math.random() * (strings.length - 1));
            }
            let word = strings[wordIndex];
            usedStrings[usedStrings.length] = word;
            const kachel = document.createElement("div");
            kachel.classList.add("kachel");
            kachel.innerText = word;
            kachel.setAttribute("onclick", "toggleStatus(this)")
            field.appendChild(kachel);
        }
    }
}
createFields();

