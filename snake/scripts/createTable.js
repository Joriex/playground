// this function is called at the beginning of the game, to create the table-fields
const createTable = (size) => {
    for (let row = 0; row < size; row++) {
        //create a new row for the table
        const newTableRow = document.createElement("tr");
        newTableRow.classList.add("pitch__row");

        for (let col = 0; col < size; col++) {
            //create new cell for the table row
            const newTableCell = document.createElement("td");
            newTableCell.classList.add("pitch__cell");
            //first col then row so there is no confusion concerning x,y coordinates
            newTableCell.id = col.toString() + "-" +  row.toString();
            newTableRow.appendChild(newTableCell);
        }
        table.appendChild(newTableRow);
    }
}