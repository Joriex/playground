

const showControls = (controlsDiv) => {
    if(controlsDiv.classList.contains("controls--deactivated")){
        controlsDiv.style.display = "none";
        controlsDiv.style.visibility = "hidden";

        const controlsActivated = document.getElementsByClassName("controls--activated")[0];
        controlsActivated.style.visibility="visible";
        controlsActivated.style.display="block";
    }
    else{
        controlsDiv.style.display = "none";
        controlsDiv.style.visibility = "hidden";

        const controlsDeactivated = document.getElementsByClassName("controls--deactivated")[0];
        controlsDeactivated.style.visibility="visible";
        controlsDeactivated.style.display="flex";
    }
}