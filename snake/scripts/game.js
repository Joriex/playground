const table = document.getElementById("snake__pitch");
let size;
//setup high score and current score
const scoreElement = document.getElementById("score");
const highScoreElement = document.getElementById("highScore");

let score = 0;
let highScore = (localStorage.getItem("highScore") !== null) ? localStorage.getItem("highScore") : 0;
let oldHighScore = highScore;

highScoreElement.innerText = highScore.toString();
//status if game is paused
let paused = true;

// id for the game Interval to clear it
let gameIntervalId;

// north = 0, east = 1, south = 2, west = 3
let currentSnakeDirection = 0;
// needed to set the right direction of new Snake Parts when eating a fruit in a corner
let oldLastSnakePartDirection = 0;

/*
 snake Object in form of array
 for each field of the snake there is one array as element of the whole snake array

 first information ([0]) is the id of the current field in which the part of the snake is currently at
 second information ([1]) is the direction in form of an object containing x and y
 */
let snake = [[{x: 2, y: 3}, 3]]

let fruit = {x: 0, y: 0}

//game happens in this function
const gameLoop = () => {
    // check if player hit a fruit if that is the case, add new snake part and set new fruit
    if (checkFruit()) {
        addSnakePath();
        setNewFruit();
    }
    document.getElementById(fruit.x.toString() + "-" + fruit.y.toString()).style.backgroundImage = 'url("textures/fruit.svg")';
    for (let snakeField = snake.length - 1; snakeField >= 0; snakeField--) {
        // gets the status of the cell before making the new move
        const currentCell = document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString());
        // clean up styles behind the snake
        if (snakeField === snake.length - 1) {
            currentCell.style.background = null;
            currentCell.style.transform = null;
        }
        //edge case for the head of the snake --> needs to check if the snake dies
        if (snakeField === 0) {
            //make movement with currentDirection
            makeMovement(snakeField, currentSnakeDirection);
            snake[0][1] = currentSnakeDirection;

            //check for death
            if (checkDeath()) endGame();

            //style for the new head table cell
            document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString()).style.backgroundImage = "url('textures/head.svg')";

            //rotate the texture
            rotateTextures(snakeField, document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString()));
        }
        // body parts and tail
        else {
            //make movement
            makeMovement(snakeField, snake[snakeField][1]);

            //save old direction for texture rotation
            const oldDirection = snake[snakeField][1];

            //set new direction of the field
            snake[snakeField][1] = snake[snakeField - 1][1];

            //update Texture if it needs to be a turn texture
            if (oldDirection !== snake[snakeField][1]) {
                setTurnTexture(snake[snakeField], snake[snakeField][1], oldDirection);
            }
            //set normal body texture if this field is not in a corner
            else {
                document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString()).style.backgroundImage = "url('textures/body.svg')";
            }

            //if it is the tail, set tail and rotate texture
            if (snakeField === snake.length - 1) {
                document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString()).style.backgroundImage = "url('textures/tail.svg')";
                rotateTextures(snakeField, document.getElementById(snake[snakeField][0].x.toString() + "-" + snake[snakeField][0].y.toString()));
            }
        }
    }
}
//makes the movement of the snake
const makeMovement = (snakeField, snakeDirection) => {
    // set new direction of the last snake Path
    if (snakeField === (snake.length - 1)) {
        oldLastSnakePartDirection = snakeDirection;
    }
    switch (snakeDirection) {
        case 0:
            snake[snakeField][0].y -= 1;
            break;
        case 1:
            snake[snakeField][0].x += 1;
            break;
        case 2:
            snake[snakeField][0].y += 1;
            break;
        case 3:
            snake[snakeField][0].x -= 1;
            break;
    }
}
//returns true if snake dies and false if it stays alive
const checkDeath = () => {
    //cover borders left and right
    if (snake[0][0].x > size - 1 || snake[0][0].x < 0) {
        return true;
    }
    if (snake[0][0].y > size - 1 || snake[0][0].y < 0) {
        return true;
    }
    const backgroundImage = document.getElementById(snake[0][0].x.toString() + "-" + snake[0][0].y.toString()).style.backgroundImage;

    if (backgroundImage.length !== 0) {
        //fruit edge case
        return backgroundImage !== 'url("textures/fruit.svg")';

    }
    if (snake.length > 1) {
        return snake[0][0].x === snake[1][0].x && snake[0][0].y === snake[1][0].y;
    }

    return false;
}

//checks of head of snake hits a fruit
const checkFruit = () => {
    return snake[0][0].x === fruit.x && snake[0][0].y === fruit.y;
}

const setNewFruit = () => {
    scoreElement.innerText = score.toString();
    if (score >= highScore) {
        highScoreElement.innerText = score.toString();
    }
    score += 1;
    document.getElementById(fruit.x.toString() + "-" + fruit.y.toString()).style.backgroundImage = "";
    do {
        fruit.x = Math.round(Math.random() * (size - 1));
        fruit.y = Math.round(Math.random() * (size - 1));
    } while (document.getElementById(fruit.x.toString() + "-" + fruit.y.toString()).style.backgroundImage.length !== 0);
    document.getElementById(fruit.x.toString() + "-" + fruit.y.toString()).style.backgroundImage = 'url("textures/fruit.svg")';
}

// add new Path to snake when hitting a fruit
const addSnakePath = () => {
    let x = snake[snake.length - 1][0].x;
    let y = snake[snake.length - 1][0].y;
    let direction = (x === (size - 1) || x === (0) || y === (size - 1) || y === (0)) ? oldLastSnakePartDirection : snake[snake.length - 1][1];
    switch (direction) {
        case 0:
            y += 1;
            break;
        case 1:
            x -= 1;
            break;
        case 2:
            y -= 1;
            break;
        case 3:
            x += 1;
            break;
    }
    snake.push([{x: x, y: y}, direction]);
}

//when player dies
const endGame = () => {
    clearInterval(gameIntervalId);
    if (score > oldHighScore) {
        // Save data to localStorage
        localStorage.setItem("highScore", (score - 1).toString());
    }
    window.location.reload();
}

//changes direction whe key is pressed
const changeDirection = (key) => {
    if (key === 'w' || key === "ArrowUp") {
        if (currentSnakeDirection !== 2 || gameIntervalId === undefined) currentSnakeDirection = 0;
    }
    if (key === 'd' || key === "ArrowRight") {
        if (currentSnakeDirection !== 3 || gameIntervalId === undefined) currentSnakeDirection = 1;
    }
    if (key === 's' || key === "ArrowDown") {
        if (currentSnakeDirection !== 0 || gameIntervalId === undefined) currentSnakeDirection = 2;
    }
    if (key === 'a' || key === "ArrowLeft") {
        if (currentSnakeDirection !== 1 || gameIntervalId === undefined) currentSnakeDirection = 3;
    }
    if (key === "Escape") {
        if (paused) {
            gameIntervalId = setInterval(gameLoop, 100);
        } else {
            clearInterval(gameIntervalId)
        }
        paused = !paused;
    }
    snake[0][1] = currentSnakeDirection
}

const startGame = () => {
    size = document.getElementById("size__input").value;
    if (!(size >= 10) || !(size <= 50)) {
        alert("Die Größe des Spielfeldes sollte zwischen 10 und 50 liegen!!!!!!!!!");
        return;
    }

    //create Field
    createTable(size);

    //make intro invisible
    const intro = document.getElementsByClassName("intro")[0];
    intro.style.display = "none";
    intro.style.visibility = "hidden";

    //make table visible
    table.style.display = "block";

    // set first fruit
    setNewFruit();

    document.getElementById(snake[0][0].x.toString() + "-" + snake[0][0].y.toString()).style.background = "url('textures/head.svg')";

    // add Listener for start of the game and direction change
    window.addEventListener(("keydown"), (e) => {
        e.preventDefault();
        changeDirection(e.key);
        gameIntervalId = setInterval(gameLoop, 90);
        paused = false;
        window.addEventListener(("keydown"), (e) => {
            changeDirection(e.key);
        }, {once: false});
    }, {once: true});
}

/**
 * prevents scrolling with arrows
 * @param e event
 */
document.onkeydown = function(e) {
    const keyCode = e.key;
    if (keyCode === "ArrowUp" || keyCode === "ArrowDown") {
        e.preventDefault();
    }
};