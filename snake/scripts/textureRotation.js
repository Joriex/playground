// texture Rotations so that the snake textures look right
const setTurnTexture = (field, newDirection, oldDirection) => {
    const tableCell = document.getElementById(field[0].x.toString() + "-" + field[0].y.toString());
    tableCell.style.backgroundImage = "url('textures/turn.svg')";
    if(oldDirection === 0){
        switch (newDirection){
            case 1:
                tableCell.style.transform = "rotate(180deg)";
                console.log(tableCell.style.transform);
                break;
            case 3:
                tableCell.style.transform = "rotate(270deg)";
                break;
            case 2:
                tableCell.style.transform = "";
                break;
        }
        return;
    }
    if(oldDirection === 1){
        switch (newDirection){
            case 2:
                tableCell.style.transform = "rotate(270deg)";
                break;
            case 0:
                tableCell.style.transform = "";
                break;
            case 3:
                tableCell.style.transform = "";
                break;
        }
        return;
    }
    if(oldDirection === 2){
        switch (newDirection){
            case 1:
                tableCell.style.transform = "rotate(90deg)";
                break;
            case 3:
                tableCell.style.transform = "rotate(0deg)";
                break;
            case 2:
                tableCell.style.transform = "";
                break;
        }
        return;
    }
    if(oldDirection === 3){
        switch (newDirection){
            case 0:
                tableCell.style.transform = "rotate(90deg)";
                break;
            case 2:
                tableCell.style.transform = "rotate(-180deg)";
                break;
            case 3:
                tableCell.style.transform = "";
                break;
        }
    }
}


const rotateTextures = (snakeField, currentCell) => {

    switch (snake[snakeField][1]) {
        case 0:
            currentCell.style.transform = "rotate(0deg)";
            break;
        case 1:
            currentCell.style.transform = "rotate(90deg)";
            break;
        case 2:
            currentCell.style.transform = "rotate(180deg)";
            break;
        case 3:
            currentCell.style.transform = "rotate(270deg)";
            break;
    }
}