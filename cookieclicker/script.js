let score = 0;
let money = 0;

let auto_money_per_sec = 0;
let score_per_click = 1;
// powerUps --> powerUp name, level, cost
let powerUps = [["increasePassiveIncome", 0, 10],["increaseIncomePerClick", 0, 40]]

const score_div = document.getElementById("score");
const money_div = document.getElementById("shop__money");

const cookieClick = () => {
    score+= score_per_click;
    money+= score_per_click;
}


const updateScore = () => {
    score += auto_money_per_sec / 1000;
    money += auto_money_per_sec / 1000;
    score_div.innerText = score.toFixed(2).toString();
    money_div.innerText = money.toFixed(2).toString();
    for(let i = 0; i < powerUps.length; i++){
        if(money >= powerUps[i][2]){
            document.getElementById("shop__upgrade-" + i).style.opacity = "1";
        }
    }
}

const powerUp = (index) => {
    if (index === 0) {
        if (money >= powerUps[index][2]) {
            //increase level of powerUp
            powerUps [index][1]++;
            // set visual display to new level
            document.getElementById("powerUp__" + index + "__level").innerText = powerUps [index][1].toString();
            // set powerUp action
            auto_money_per_sec += 5 * powerUps[index][1] * Math.ceil(powerUps[index][1] / 10);
            // decrease money by cost of PowerUp
            money -= powerUps[index][2];
            // increase cost of powerUp
            powerUps[index][2] *= 2;
            // visualize new upgrade cost
            document.getElementById("powerUp__" + index + "__cost").innerText = powerUps [index][2].toString();
        }
    }
    if (index === 1) {
        if (money >= powerUps[index][2]) {
            //increase level of powerUp
            powerUps [index][1]++;
            // set visual display to new level
            document.getElementById("powerUp__" + index + "__level").innerText = powerUps [index][1].toString();
            // set powerUp action
            score_per_click += 2 * powerUps[index][1] * Math.ceil(powerUps[index][1] / 10);
            // decrease money by cost of PowerUp
            money -= powerUps[index][2];
            // increase cost of powerUp
            powerUps[index][2] *= 4;
            // visualize new upgrade cost
            document.getElementById("powerUp__" + index + "__cost").innerText = powerUps [index][2].toString();
        }
    }
    //set opacity again
    for(let i = 0; i < powerUps.length; i++){
        if(money < powerUps[i][2]){
            document.getElementById("shop__upgrade-" + i).style.opacity = "0.6";
        }
    }
}
setInterval(updateScore);